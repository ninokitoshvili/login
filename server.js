const express = require("express");



const bodyParser =require('body-parser')


const port = 8000;
const app = express();


app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine', 'ejs');
app.set("views", "./view")

var urlencodedParser = bodyParser.urlencoded({extended:false})

app.use('/assets',express.static('assets'))
app.use('/view',express.static('view'))


app.get("/", (req,res) => {
    res.render("welcome");
});
app.get("/registration", (req,res) => {
    res.render("registration.ejs");
});

app.get("/registration/fizikuri", (req,res) => {
    res.render("registrationFizikuri.ejs");
});

app.get("/registration/kompania", (req,res) => {
    res.render("registrationKompania.ejs");
});





app.listen(port, () => {
    console.log(`server runs at http://localhost:${port}`);
})